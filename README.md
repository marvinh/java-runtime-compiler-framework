# Java Runtime Compiler Framework
## Description
This is a framework that allows its users to compile Java code at runtime into class files. It is a spin-off of the
[Plugin Loader](https://gitlab.com/marvinh/plugin-system-for-java) library from marvinh which in previous version 
had this function included.

## Requirements
Java applications that use this artifact need to be run on a JDK, because this library uses the Java Compiler Toolkit
which is not available on the normal JRE.

## How to use
Just add the artifact to the dependencies section of your projects pom.xml:
```
<dependency>
    <<groupId>de.osshangar</groupId>
    <artifactId>java-runtime-compiler-framework</artifactId>
    <version>1.0</version>
</dependency>
```
Now within your Java code load the source code to compile from a file or input stream into 
a String variable and call the compiler framework:
```
byte[] byteCode = Compiler.compile(className, sourceCode);
```
The className parameter is the full qualified name of the  class to compile and the sourceCode
parameter is the String variable containing the source code to compile.

If you now want to load the compiled class I suggest to use the PluginClassLoader from the
[Java Plugin Framework](https://search.maven.org/artifact/de.osshangar/java-plugin-framework):
```
PluginClassLoader classLoader = new PluginClassLoader(Collections.singletonMap(className, byteCode));
Class<?> loadedClass = classLoader.findClass(className);
```
You can now use Reflection to construct an instance of the class:
```
Constructor<?> constructor = loadedClass.getConstructor(constructorSignature);
Object instance = constructor.newInstance(constructorArguments);
```

## How to build
### Building the artifact
To build the artifacts package just run
```
mvn clean package
```