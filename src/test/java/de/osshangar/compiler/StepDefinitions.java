package de.osshangar.compiler;

import de.osshangar.plugin.classloader.PluginClassLoader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.NonNull;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class StepDefinitions {
    private static final String sourceFileFolder = "testsources";

    private final Map<String, byte[]> byteCodes = new HashMap<>();

    private Class<?> loadedClass;
    private Object instance;

    @Then("compile class {string} of source file named {string}")
    public void compileSourceFileNamed(@NonNull String className, @NonNull String sourceFileName) throws CompilerException, IOException {
        File sourceFile = new File(sourceFileFolder, sourceFileName);
        InputStream sourceInputStream = ClassLoader.getSystemResourceAsStream(sourceFile.toString());
        assertNotNull(sourceInputStream);
        String sourceCode = IOUtils.toString(sourceInputStream, StandardCharsets.UTF_8);
        byte[] byteCode = Compiler.compile(className, sourceCode);
        assertTrue(byteCode.length > 0);
        byteCodes.put(className, byteCode);
    }

    @And("instantiate class {string}")
    public void instantiateClass(@NonNull String className) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        assert byteCodes.containsKey(className);
        byte[] byteCode = byteCodes.get(className);
        PluginClassLoader classLoader = new PluginClassLoader(Collections.singletonMap(className, byteCode));
        loadedClass = classLoader.findClass(className);
        assertNotNull(loadedClass);
        Constructor<?> constructor = loadedClass.getConstructor();
        assertNotNull(constructor);
        instance = constructor.newInstance();
        assertNotNull(instance);
    }

    @Then("execute method {string} with result {string}")
    public void executeMethodWithResult(@NonNull String methodName, String expectedResult) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        assert loadedClass != null;
        assert instance != null;
        Object result = loadedClass.getMethod(methodName).invoke(instance);
        if (expectedResult == null){
            assertNull(result);
        }
        else {
            assertEquals(expectedResult, result.toString());
        }
    }

    @When("fail compile class {string} of source file named {string} with exception {string}")
    public void failCompileClassOfSourceFileNamedWithException(String className, String sourceFileName, String exceptionClassName) throws IOException {
        File sourceFile = new File(sourceFileFolder, sourceFileName);
        InputStream sourceInputStream = ClassLoader.getSystemResourceAsStream(sourceFile.toString());
        assertNotNull(sourceInputStream);
        String sourceCode = IOUtils.toString(sourceInputStream, StandardCharsets.UTF_8);
        try {
            Compiler.compile(className, sourceCode);
            fail(String.format("Expected exception %s was never thrown", exceptionClassName));
        } catch (Exception e) {
            assertEquals(exceptionClassName, e.getClass().getCanonicalName());
        }
    }
}
