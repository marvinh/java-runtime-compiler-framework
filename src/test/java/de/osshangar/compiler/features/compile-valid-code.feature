Feature: Compile valid code
  Rule: Valid source codes should be compiled correctly and should be loadable by the class loaders
    Scenario Outline: Compile valid source file
      When compile class "<className>" of source file named "<sourceFileName>"
      And instantiate class "<className>"
      Then execute method "<methodName>" with result "<result>"

      Examples:
        | sourceFileName  | className  | methodName      | result |
        | HelloWorld.java | testsources.HelloWorld | printHelloWorld | Hello World |

    Scenario Outline: Compile source file with mismatching root class name
      When compile class "<className>" of source file named "<sourceFileName>"
      And instantiate class "<className>"
      Then execute method "<methodName>" with result "<result>"

      Examples:
        | sourceFileName  | className  | methodName      | result |
        | HelloWorldRenamed.java | testsources.HelloWorld | printHelloWorld | Hello World |
