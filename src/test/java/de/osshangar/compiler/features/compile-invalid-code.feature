Feature: Compile invalid code
  Rule: Source files with syntax errors should throw an exception
    Scenario Outline: Compile source file with wrong return type
      When fail compile class "<className>" of source file named "<sourceFileName>" with exception "<exceptionClass>"

      Examples:
        | sourceFileName  | className  | exceptionClass      |
        | HelloWorldWrongReturnType.java | testsources.HelloWorld | de.osshangar.compiler.CompilerException |

    Scenario Outline: Compile source file with wrong return type
      When fail compile class "<className>" of source file named "<sourceFileName>" with exception "<exceptionClass>"

      Examples:
        | sourceFileName  | className  | exceptionClass      |
        | HelloWorldMissingReturnStatement.java | testsources.HelloWorld | de.osshangar.compiler.CompilerException |

