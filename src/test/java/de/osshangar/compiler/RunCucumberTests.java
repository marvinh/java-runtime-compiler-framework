package de.osshangar.compiler;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/de/osshangar/compiler/features")
public class RunCucumberTests {
}
