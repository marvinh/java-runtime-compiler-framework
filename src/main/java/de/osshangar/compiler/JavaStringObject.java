package de.osshangar.compiler;

import javax.tools.SimpleJavaFileObject;
import java.net.URI;

public class JavaStringObject extends SimpleJavaFileObject {
    private final String code;

    public JavaStringObject(String className, String code) {
        super(URI.create(String.format(
                "string:///%s%s",
                className.replace('.','/'),
                Kind.SOURCE.extension
        )), Kind.SOURCE);
        this.code = code;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        return code;
    }

}
