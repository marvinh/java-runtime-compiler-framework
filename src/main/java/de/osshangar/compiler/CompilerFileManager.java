package de.osshangar.compiler;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import java.io.IOException;

public class CompilerFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
    private final JavaFileObject javaFileObject;

    public CompilerFileManager(StandardJavaFileManager fileManager, JavaFileObject javaFileObject) {
        super(fileManager);
        this.javaFileObject = javaFileObject;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
        return javaFileObject;
    }
}

