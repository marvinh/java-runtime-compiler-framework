package de.osshangar.compiler;

import javax.tools.SimpleJavaFileObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

public class JavaByteObject extends SimpleJavaFileObject {
    private ByteArrayOutputStream outputStream;

    public JavaByteObject(String name) {
        super(URI.create(String.format("bytes:///%s%s", name, name.replaceAll("\\.", "/"))), Kind.CLASS);

    }

    @Override
    public OutputStream openOutputStream() throws IOException {
        this.outputStream = new ByteArrayOutputStream();
        return outputStream;
    }

    public byte[] getBytes() {
        return outputStream.toByteArray();
    }

}
