package de.osshangar.compiler;

import javax.tools.*;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Compiler {
    public static byte[] compile(String className, String sourceCode) throws CompilerException {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        JavaByteObject javaByteObject = new JavaByteObject(className);
        CompilerFileManager compilerFileManager = new CompilerFileManager(compiler.getStandardFileManager(diagnostics, null, null), javaByteObject);
        List<String> options = Collections.emptyList();
        JavaCompiler.CompilationTask compilationTask = compiler.getTask(
                null, compilerFileManager, diagnostics,
                options, null, () -> {
                    JavaFileObject javaFileObject = new JavaStringObject(className, sourceCode);
                    return Collections.singletonList(javaFileObject).iterator();
                });
        boolean compilationSuccessful = compilationTask.call();
        if (!compilationSuccessful){
            String message = diagnostics.getDiagnostics().stream().map(Object::toString).collect(Collectors.joining());
            throw new CompilerException(String.format("Failed to compile class '%s':\n%s", className, message));
        }
        return javaByteObject.getBytes();
    }
}
