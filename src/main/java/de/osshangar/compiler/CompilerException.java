package de.osshangar.compiler;

public class CompilerException extends Exception{
    public CompilerException(String message) {
        super(message);
    }
}
